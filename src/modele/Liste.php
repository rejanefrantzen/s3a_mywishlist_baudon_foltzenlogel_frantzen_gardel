<?php


namespace mywishlist\modele;


class Liste extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'liste';
    protected $primaryKey = 'id';
    public $timestamps=false;

//Fonction permettant l'affichage des items d'une liste
    public function items(){
       return $this->hasMany('mywishlist\modele\Item', 'liste_id', 'no');
    }

    public function __toString(){
      $app =\Slim\Slim::getInstance();
      $rootUri = $app->request->getRootUri();
      $itemUrl = $app->urlFor('route_liste', ['id'=> $this->no] );

      $l='<div class="liste">
          <h4 class="titre_liste">'.$this->titre.'</h4>
          <div class="descript_item">'.$this->description.'</div><br>
          <div class="expiration_item">Expiration : '. $this->expiration.'</div><br>
          </div>';
      return $l;
    }

}
