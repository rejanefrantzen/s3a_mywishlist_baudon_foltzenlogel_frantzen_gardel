<?php
namespace mywishlist\controleur;
use mywishlist\modele\Item;
use mywishlist\modele\Liste;
use mywishlist\modele\User;

use mywishlist\vue\VueParticipant;
use Illuminate\Database\Capsule\Manager as DB;

class ControleurListe {

    //Fonction permettant d'aller a la page d'accueuil du site
  public function getAccueil(){
    $v=new VueParticipant(NULL, VueParticipant::HOME_VIEW);
    $v->render();
  }

  //Fonction permettant d'obtenir les items d'une liste grace a son id
  public function getListeById($id){
  $liste = Liste::where("no", "=", $id)->first();
  $tabVal=array($liste);
  $items=$liste->items()->get();
  foreach ($items as $item) {
    array_push($tabVal, $item);
  }
  $v=new VueParticipant($tabVal, VueParticipant::LIST_VIEW);
  $v->render();
  }

//Fonction permettant d'obtenir les items d'une liste grace a son token
  public function getListeByToken($token){
    $liste = Liste::where("token", "=", $token)->first();
    $tabVal=array($liste);
    $items=$liste->items()->get();
    foreach ($items as $item) {
      array_push($tabVal, $item);
    }
    $v=new VueParticipant($tabVal, VueParticipant::LIST_VIEW);
    $v->render();
  }

//Fonction permettant de voir toutes les listes qui peuvent etre vues
  public function getAllListe(){
    $liste = Liste::all();
    $v=new VueParticipant($liste,  VueParticipant::ALL_LIST_VIEW);
    $v->render();
  }

  //Fonction permettant de creeer une liste
  public function createListe(){
    $v=new VueParticipant(NULL, VueParticipant::CREATE_LIST);
    $v->render();
  }


/*
* methode de création d'une liste,
* recuperation des informations du formulaire et
* creation d'une nouvelle liste dans la base de données
* creation d'un cookie pour identifier le createur de la liste
*/
  public function addListe($tab){
    $l=new Liste();
    $l->titre=$tab->post('titre_liste');
    $l->description=$tab->post('description_liste');
    $l->expiration=$tab->post('dateexpi_liste');
    $l->token=uniqid();

    if (!isset($_COOKIE["userid"])) {//si pas de cookies creation nouveau user + creation cookie
      $u=new User();
      $u->save();
      setcookie("userid", $u->user_id, time()+365*3600*24, "/");
      $l->user_id=$u->user_id;
    }else {
      $l->user_id=$_COOKIE["userid"];
    }
    $l->save();
    $l=Liste::where('titre','=',$tab->post('titre_liste'))->first();

    $v=new VueParticipant(array($l), VueParticipant::LIST_VIEW);
    $v->render();
  }

//Fonction permettant d'obtenir les parametres d'une liste grace a son token
  public function getParamListe($token){
    $liste = Liste::where("token", "=", $token)->first();
    $v=new VueParticipant($liste, VueParticipant::LIST_PARAM_VIEW);
    $v->render();
  }

  //Fonction permettant d'ajouter un item a une liste
  public function addItemListe($token){
    $liste = Liste::where("token", "=", $token)->first();
    $v=new VueParticipant($liste, VueParticipant::LIST_ADD_ITEM_VIEW);
    $v->render();
  }

  //Fonction permettant de pouvoir modifier une liste grace a son token
  public function getModifListe($token){
    $liste = Liste::where("token", "=", $token)->first();
    $v=new VueParticipant($liste, VueParticipant::LIST_MODIF_VIEW);
    $v->render();
  }

  //Fonction permettant d'effectuer les modifications realisees precedemment sur une liste grace a son token
  public function setModif($token, $tab){
    $description='';
    if ($tab->post('description_liste')==='') {
      $l=Liste::where('token', '=', $token)->first();
      $description=$l->description;
    }else {
      $description=$tab->post('description_liste');
    }
    $l=Liste::where('token', '=', $token)->update(['titre'=>$tab->post('titre_liste'),
                                                   'description'=>$description,
                                                   'expiration'=>$tab->post('expiration_liste')]);

    $app =\Slim\Slim::getInstance();
    $urlListe = $app->urlFor('route_liste_token', array('token' => $token));
    header('Location:http://'.$_SERVER['HTTP_HOST'].$urlListe);
    exit;
  }


  public function supprListe($token){
      $app =\Slim\Slim::getInstance();
      $urlListes=$app->urlFor('route_allliste', array('token' => $token));

      Liste::where('token','=', $token)->delete();
      header('Location:http://'.$_SERVER['HTTP_HOST'].$urlListes);
      exit;
  }

}
