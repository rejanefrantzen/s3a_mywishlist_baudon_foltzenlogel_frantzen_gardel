<?php
namespace mywishlist\controleur;
use mywishlist\modele\Item;
use mywishlist\modele\Liste;
use mywishlist\modele\User;

use mywishlist\vue\VueParticipant;

class ControleurItem {

 // Obtenir un item a partir de son id afin de l'identifier
  public function getItemById($id){
  $item = Item::where("id", "=", $id)->first();
  $v=new VueParticipant($item, VueParticipant::ITEM_VIEW);
  $v->render();
  }

  //Obtenir les items d'une liste
  public function getItemListe($idItem,$idListe){
    $item = Item::where("id", "=", $idItem)->where('liste_id','=',$idListe)->first();
    $v=new VueParticipant($item, VueParticipant::LIST_ITEM_VIEW);
    $v->render();
  }
// Obtenir les items d'une liste en fonction du token et de l'id de la liste
  public function getItemIdListeToken($token, $id){
    $liste=Liste::where('token','=',$token)->first();
    $item=Item::where('liste_id','=',$liste->no)->where('id','=',$id)->first();
    $v=new VueParticipant($item, VueParticipant::ITEM_VIEW);
    $v->render();
  }

  // Fonction permettant de pouvoir changer un item et ses caractéristiques dans une liste
  public function changeItem($token, $id, $tab){
      $i = Item::where("id", "=", $id)->first();
      $i->nom=$tab->post('item_nom');
      $i->tarif=$tab->post('item_tarif');
      $i->descr=$tab->post('description_item');
      $i->url=$tab->post('item_url');
      $i->save();

      $app =\Slim\Slim::getInstance();
      $urlListe = $app->urlFor('route_item_liste', array('token' => $token, 'id'=>$id));
      echo $urlListe;
      header("Location:http://".$_SERVER['HTTP_HOST'].$urlListe);
      exit;
  }

  //Fonction permettant d'ajouter un item a une liste
  public function addItem($tokenListe,$tab){
      $liste=Liste::where('token','=',$tokenListe)->first();
      $idListe= $liste->no ;
      echo $tab->post('description_item');
      $i=new Item();
      $i->nom=$tab->post('item_nom');
      $i->tarif=$tab->post('item_tarif');
      $i->descr=$tab->post('description_item');
      $i->url=$tab->post('item_url');
      $i->reserv=NULL;
      $i->liste_id=$idListe;
      $i->save();

      // $v=new VueParticipant($i, VueParticipant::ITEM_VIEW);
      // $v->render();
      $app =\Slim\Slim::getInstance();
      $urlListe = $app->urlFor('route_liste_token', array('token' => $tokenListe));
      echo $urlListe;
    header("Location:http://".$_SERVER['HTTP_HOST'].$urlListe);
    exit;
  }

// Fonction permettant de pouvoir reserver un item dans une liste parmi plusieurs items
  public function reserverItem($id, $token, $tab){
      $u=NULL;
    if (!isset($_COOKIE["userid"])) {
      $u=new User();
      $u->nom=$tab->post('nom_reserv');
      $u->save();
      setcookie("userid",$u->user_id, time()+365*24*3600,"/");
    }else {
      $u=User::where('user_id', '=', $_COOKIE["userid"])->first();
      echo $u->nom;
      $u->nom=$tab->post('nom_reserv');
      $u->save();
    }
    $i=Item::where('id','=',$id)->first();
    $i->reserv=$u->user_id;
    $i->message=$tab->post('message_reserv');
    $i->save();

    $l=Liste::where('no','=', $i->liste_id);
    $app =\Slim\Slim::getInstance();
    $urlListe = $app->urlFor('route_liste_token', array('token' => $token));
    header('Location:http://'.$_SERVER['HTTP_HOST'].$urlListe);
    exit;
  }

  // Fonction permettant a l'utilisateur les modifications effectuees sur un item
    public function getModifItem($id){
        $item = Item::where("id", "=", $id)->first();
        $v=new VueParticipant($item, VueParticipant::ITEM_MODIF_VIEW);
        $v->render();
    }
}
