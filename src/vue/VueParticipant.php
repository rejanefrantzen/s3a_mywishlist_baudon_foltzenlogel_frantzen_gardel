<?php
namespace mywishlist\vue;
use mywishlist\modele\Liste;
use mywishlist\modele\User;

class VueParticipant{

    const ITEM_VIEW = 1;
    const LIST_VIEW = 2;
    const ALL_LIST_VIEW = 3;

    const LIST_VIEW_TOKEN = 4;
    const LIST_ITEM_VIEW = 5;
    const CREATE_LIST = 6;
    const HOME_VIEW = 8;

    const LIST_PARAM_VIEW = 7;
    const LIST_ADD_ITEM_VIEW=9;
    const LIST_MODIF_VIEW=10;
    const ITEM_MODIF_VIEW = 11;

  private $val;
  private $selecteur;
  function __construct($v, $s){
    $this->val=$v;
    $this->selecteur=$s;
  }

  //affiche de la page d'accueil du site
  public function htmlAccueil(){
    $app =\Slim\Slim::getInstance();

    $urlCreation=$app->urlFor('route_create_liste');
    $content="<h2>Accueil</h2>
    <div class='pres_site'><p><strong>MyWishlist</strong> est un site de création de listes de souhaits.<br>
    Créez une liste pour votre anniversaire, pour Noël ou pour toute autre occasion.<br>
    Une fois votre liste créée et tous vos souhaits ajoutés, partagez votre liste avec vos amis.<br>
    Ils pourront réserver des items de votre liste qu'ils pourront ainsi vous offrir.
     Adieu la déception, dites bonjour à notre solution ! .<br>
    Commencez en créant votre première liste <a href='$urlCreation'><strong>ici !</strong></a></p></div>";
    return $content;
  }
  //affichage d'un item seul
  public function htmlItem(){
    $app =\Slim\Slim::getInstance();
    $content= "<div class='affiche_item'>".$this->val->__toString();
    $l=Liste::where('no','=',$this->val->liste_id)->first();
    $urlListeItem=$app->urlFor('route_reservation',['token'=>$l->token, 'id'=> $this->val->id]);
    $etatReserv="";

    $content.="<div class='info_reserv'>";

      if ($this->val->reserv==NULL) {

        if ( (isset($_COOKIE["userid"]) && $_COOKIE["userid"]==$l->user_id)) {
            $content.="<p>Cette item n'a pas encore été résevé.</p>";
            $urlParamItem= $_SERVER['REQUEST_URI'].'/modif';
            $content.="<div id='parametres'><a class='bouton' href='$urlParamItem'>Modifier</a></div>";
        }else {

          $content.="<form action='$urlListeItem' method='POST'>";

          if (isset($_COOKIE["userid"])) {
            $userCookie=User::where('user_id','=',$_COOKIE['userid'])->first();
            $nom=$userCookie->nom;
            $content.="<input type='text' name='nom_reserv' placeholder='Votre nom' value=$nom>";
          }else {
            $content.="<input type='text' name='nom_reserv' placeholder='Votre nom'>";
          }

          $content.="<textarea rows='5' cols='33' name='message_reserv' placeholder='Ajouter un message'></textarea><br>
          <input class='bouton' type='submit' value='Resever'>
          </form>";
        }

      }else {
        if ($l->expiration<=date("Y-m-d")) {
          $u=User::where('user_id','=',$this->val->reserv)->first();
          $message=$this->val->message;
          $nomUser=$u->nom;
          $d=date("Y-m-d");
          $content.="<p>Cet item a été reservé par <strong id='nom_reserv'>$nomUser</strong></p>
                    <p id='message_reserv'>\"$message\"</p>";
        }else {
          $content.='<strong><p>Cette item a déjà été réservé';

          if (isset($_COOKIE['userid']) && $_COOKIE['userid']==$l->user_id) {//affiche de du nom de la reservation que pour les visiteur
            $content.=".";
          }else {
            $u=User::where('user_id', '=',$this->val->reserv)->first();
            $content.=" par ".$u->nom;
          }
        }
        $content.="</strong></p></div></div>";
      }
     return $content;
     }

  //affichage d'une liste : liste + items de la liste
  // affichage de l'etat de la reservation
  // un bouton "Parametres de ma liste" apparait si vous etes le createur de la liste
  public function htmlListeItem(){
    $res="";$tokenListe=NULL;
    $app =\Slim\Slim::getInstance();
    $rootUri = $app->request->getRootUri();
    $l=NULL;
    foreach ($this->val as $val){
      if ($val->titre!=null) {//si c'est la liste
        $res.=$val->__toString();
        $tokenListe=$val->token;
        $l=$val;

        if (isset($_COOKIE['userid']) && $_COOKIE['userid']==$val->user_id) {//si c'est le createur de la liste
          $urlParamListe='http://'.$_SERVER['HTTP_HOST'].$app->urlFor('route_param_liste', ['token' => $val->token]);

          $res.="<div id='parametres'><a class='bouton' href='$urlParamListe'>Parametres</a></div>";
          $urlListe=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
          $res.="<p>Envoyer votre liste à vos proches : $urlListe</p>";
        }

        $res.="<div class='items'>";

      }else {
        $img="";
        if ($val->img==NULL) {//si l'item n'a pas d'image mettre l'image par default
          $img="pas_image.jpg";
        }else {
          $img=$val->img;
        }
        $itemUrl = $app->urlFor('route_item_liste', ['token'=>$tokenListe,'id'=> $val->id] );
        $routeImg = str_replace ('/index.php','',$_SERVER['SCRIPT_NAME'].'/img/'.$img);
        $urlItem="http://".$_SERVER['HTTP_HOST'].$itemUrl;

        $res.="<div class='item_liste'><a href='$urlItem'><h5 class='nom_item'>$val->nom</h5>";
          $res.="<img id='img_item_liste' src='$routeImg'</a>";
          $etatReserv="";

          if ($val->reserv!=NULL) {//si l'item est reseve
            $etatReserv.='<strong><p>Cette item a déjà été réservé';

              if (isset($_COOKIE['userid']) && $_COOKIE['userid']==$l->user_id) {//affiche de du nom de la reservation que pour les utilisateurs
                $etatReserv.=".";
              }else {
                $u=User::where('user_id', '=',$val->reserv)->first();
                $etatReserv.=" par ".$u->nom;
              }

              $etatReserv.="</strong></p>";
          }
        $res.= "<div class='reserv_item'>$etatReserv</div></div>";

      }
    }
    $res.='</div>';

    return $res;
  }

/*
* fonction d'affichage de toutes les listes
*/
  public function htmlAllListe(){
    $app =\Slim\Slim::getInstance();
    $rootUri = $app->request->getRootUri();
      // <a href="http://'.$_SERVER['HTTP_HOST'].$itemUrl.'">'.$this->titre.'</a>
    $res="";
    foreach ($this->val as $val){
      $res.=$val->__toString();
      $listeUrlToken = $app->urlFor('route_liste_token', ['token'=> $val->token] );
      $res.='<a class="bouton" class="bt_liste" href="http://'.$_SERVER['HTTP_HOST'].$listeUrlToken.'">Voir plus</a>';
    }
    return $res;
    // return "";
  }

  //Fonction permettant l'affichage de la page de creation d'une liste
  public function htmlCreationListe(){
    $app =\Slim\Slim::getInstance();
    $urlCreation = $app->urlFor('route_create_liste');
    $dateCourante=date('Y-m-d');
    $res=<<<END

    <form action="" method="POST">
      <div class="creationListe">
        <p>Titre : </p>
        <input type="text" name="titre_liste" placeholder="Titre de votre liste de souhaits" autofocus required><br>

        <p>Description   : </p>
        <textarea rows="5" cols="33" name="description_liste" placeholder="Décrivez votre liste de souhaits"></textarea><br>

        <p>Date d'expiration   : </p>
        <input type="date" name="dateexpi_liste"
               value="$dateCourante"
               min="$dateCourante" ><br>
        <input type="submit" value="Valider">
      </p>
    </form>
END;
    return $res;
  }

  // Fonction permettant d'afficher la page qui gere les parametres d'un item
    public function htmlParamItem(){
        $item=$this->val;

        $app =\Slim\Slim::getInstance();
        $token=Liste::where('no', '=', $item->liste_id)->first()->token;
        $urlChangeItem=$app->urlFor('route_modif_item', ['token'=>$token, 'id'=>$item->id]);
        $content =<<<END
    <h4>Modifiez un item de votre liste : </h4><br>
    <form action="$urlChangeItem" method="POST">
      <div class="creation_item">
        <p>Nom de l'item : </p>
        <input type="text" name="item_nom" value="$item->nom" autofocus required><br>

        <p>Prix : </p>
        <input type="number" step=".01" min="0" name="item_tarif" value="$item->tarif" required>

        <p>Description   : </p>
        <textarea rows="5" cols="33" name="description_item" id="descriptionArea"></textarea><br>
        <script>
            document.getElementById('descriptionArea').innerHTML="$item->descr";
        </script>
        <p>Une page où l'acheter ? : </p>
        <input type="text" name="item_url" aria-valuetext="$item->url"><br>

        <input class="bouton" type="submit"  value="Modifier" >
      </div>
    </form>
END;
        return $content;
    }

    //Fonction permettant d'afficher la page des parametres d'une liste
  public function htmlParamListe(){
    $liste=$this->val;

    $app =\Slim\Slim::getInstance();
    $urlAjouterItem=$app->urlFor('route_param_liste_add_item', ['token' => $liste->token]);
    $urlModifListe=$app->urlFor('route_param_liste_modif', ['token' => $liste->token]);
    $urlSupprListe=$app->urlFor('route_param_liste_suppr', ['token' => $liste->token]);


    $content =<<<END
    <h4>Modfier votre liste : $liste->titre<h4><br>
      <a class="bouton" href="$urlAjouterItem">Ajouter un item</a>
      <a class="bouton" href="$urlModifListe">Parametres</a>
      <a class="bouton" href="$urlSupprListe">Supprimer la liste</a>
END;
  return $content;
  }

  //Fonction permettant l'affichage de la page de l'ajout d'items a une liste
  public function htmlAjouterItemListe(){
    $app =\Slim\Slim::getInstance();
    $urlCreationListe=$app->urlFor('route_param_liste_add_item');
    $urlCreationListe=str_replace(':token',$this->val->token, $urlCreationListe);
    $nomListe= $this->val->titre;

    $content =<<<END
    <h4>Ajoutez un nouvelle item a votre liste : <strong>$nomListe</strong></h4><br>
    <form action="$urlCreationListe" method="POST">
      <div class="creation_item">
        <p>Nom de l'item : </p>
        <input type="text" name="item_nom" placeholder="Nom du nouvel item" autofocus required><br>

        <p>Prix : </p>
        <input type="number" step=".01" min="0" name="item_tarif" placeholder="Prix de l'item" required>

        <p>Description   : </p>
        <textarea rows="5" cols="33" name="description_item" placeholder="Décrivez votre liste de souhait"></textarea><br>

        <p>Une page où l'acheter ? : </p>
        <input type="text" name="item_url" placeholder="Lien de l'achat"><br>

        <input class="bouton" type="submit"  value="Ajouter" >
      </div>
    </form>
END;
    return $content;
  }

  //Fonction permettant l'affichage de la page qui permet de modifier une liste (son nom, sa description, sa date d'expiration)
  public function htmlModifListe(){
    $liste=$this->val;
    $app =\Slim\Slim::getInstance();
    $urlModifListe=$app->urlFor('route_param_liste_modif', ['token'=>$liste->token]);
    $content=<<<END
    <h4>Modifiez les informations de votre liste : </h4><br>

    <form action="$urlModifListe" method="POST">
      <p>Titre</p>
      <input type="text" name="titre_liste" value="$liste->titre">

      <p>Description</p>
      <textarea rows="5" cols="33" name="description_liste" >$liste->description</textarea><br>

      <p>Date d'expiration</p>
      <input type="date" name="expiration_liste" value="$liste->expiration"><br>

      <input class="bouton" type="submit" value="Enregistrer">
    </form>
END;

  return $content;
  }


//Fonction qui permet l'affichage d'un rendu
  public function render(){
  switch ($this->selecteur) {
    case  VueParticipant::ITEM_VIEW:
      $content=$this->htmlItem();
    break;
    case VueParticipant::ITEM_MODIF_VIEW;
        $content=$this->htmlParamItem();
    break;
    case  VueParticipant::ALL_LIST_VIEW:
      $content=$this->htmlAllListe();
    break;
    case  VueParticipant::LIST_VIEW:
      $content=$this->htmlListeItem();
    break;
    case  VueParticipant::CREATE_LIST:
      $content=$this->htmlCreationListe();
    break;
    case  VueParticipant::LIST_PARAM_VIEW:
      $content=$this->htmlParamListe();
    break;
    case VueParticipant::HOME_VIEW:
      $content=$this->htmlAccueil();
    break;
    case VueParticipant::LIST_ADD_ITEM_VIEW:
      $content=$this->htmlAjouterItemListe();
    break;
    case VueParticipant::LIST_MODIF_VIEW:
      $content=$this->htmlModifListe();
    break;


    default:
      break;
    }
    $app =\Slim\Slim::getInstance();
    $urlAccueil='http://'.$_SERVER['HTTP_HOST'].$app->urlFor('route_accueil');
    $urlCss= str_replace ('/index.php','',$_SERVER['SCRIPT_NAME']."/web/css/style.css");
    $urlCreationLitse='http://'.$_SERVER['HTTP_HOST'].$app->urlFor('route_create_liste');
    $urlAllListes='http://'.$_SERVER['HTTP_HOST'].$app->urlFor('route_allliste');

    $html=<<<END
    <!DOCTYPE html>
  <html>
    <head>
    <link rel="stylesheet" type="text/css" href="$urlCss">


    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">

    <title>MyWishlist</title>
    </head>
    <body>
    <div id='header'>
      <a href="$urlAccueil"><h1 id='titre_site'>MyWishlist</h1></a>
      <a class="bouton" id="bt_creation" href="$urlCreationLitse">Creer une liste</a>
      <a class="bouton" id="bt_creation" href="$urlAllListes">Voir les listes</a>
    </div>
      <div id=content> $content </div>
    </body>
  <html>

END;

echo $html;
  }

}
